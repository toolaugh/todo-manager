
let config = {
    PORT: process.env.PORT || '8080',
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY || 'nguyenson@bocasay.com',
    API_VERSION: 'v1',
    API_BASE_URL: '/task/api/',

    DB_URI: '',
    DB_NAME: 'task-manager-Dev',
    DB_HOST: process.env.DB_HOST || 'localhost',
    DB_PORT: 27017,
    DB_USER: process.env.DB_USER || 'root',
    DB_PASSWORD: process.env.DB_PASSWORD || '',
}

if (process.env.NODE_ENV === 'development') {
    config.DB_URI = `mongodb://${config.DB_HOST}:${config.DB_PORT}/${config.DB_NAME}`
}

if (process.env.NODE_ENV === 'staging') {
    config.DB_NAME = "todo-manager"
    config.DB_URI = `mongodb+srv://admin:1234567890@cluster0-pgvx8.mongodb.net/todo-manager?retryWrites=true`;
}
if (process.env.NODE_ENV === 'production') {
    config.DB_NAME = "todo-manager"
    config.DB_URI = `mongodb+srv://admin:1234567890@cluster0-pgvx8.mongodb.net/todo-manager?retryWrites=true`;
}
console.log('ss', process.env.NODE_ENV, config.DB_NAME)
module.exports = config