let config = {
    PORT: process.env.PORT || '8080',
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY || 'tgfdw3456yhgfdfghju654345678ijhgfdfghu876543wsdfghuji87654',

    API_VERSION: 'v1',
    API_BASE_URL: '/task/api/',

    DB_URI: '',
    DB_NAME: 'task-manager-Dev',
    DB_HOST: process.env.DB_HOST || 'localhost',
    DB_PORT: 27017,
    DB_USER: process.env.DB_USER || 'root',
    DB_PASSWORD: process.env.DB_PASSWORD || '',
}
module.exports = config