var mongoose = require('mongoose')
var config = require('./index')

let DB_URI = config.DB_URI || `mongodb://${config.DB_HOST}:${config.DB_PORT}/${config.DB_NAME}`

if (process.env.NODE_ENV === 'production') {
    DB_URL = `mongodb+srv://admin:1234567890@cluster0-pgvx8.mongodb.net/todo-manager?retryWrites=true`;
}

if (process.env.NODE_ENV === 'staging') {
    DB_URI = `mongodb+srv://admin:1234567890@cluster0-pgvx8.mongodb.net/todo-manager?retryWrites=true`;
}

mongoose.connect(DB_URI, { useNewUrlParser: true })
mongoose.set('useCreateIndex', true)
mongoose.set('useFindAndModify', false);


/* connection event */
const db = mongoose.connection

db.on('connected', function () {
    console.log('Mongo is connected with URI:', DB_URI)
})

db.on('error', function (error) {
    console.log('Connect mongo error with URI: ', DB_URI, 'error: ', error.message)
})

db.on('disconnected', function () {
    console.log('Disconnect to mongo with URI: ', DB_URI)
})