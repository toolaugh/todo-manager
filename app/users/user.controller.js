var Status = require('../../constants/SystemConstants')
var SystemCode = require('../../constants/SystemCode')
var User = require('./user.model')
var Utils = require('../../utils')

module.exports.updateUsers = function (req, res) {
  res.json({ update: 'update list Usser' })
}

module.exports.updateUser = async function (req, res) {
  const userInfo = req.body
  let { phoneNumber, email } = userInfo
  if ((email && !Utils.validateEmail(email)) || (phoneNumber && !Utils.validatePhone(phoneNumber))) {
    return res.status(400).send({
      status: Status.STATUS_ERROR,
      message: `${SystemCode.INVALID_PARAMS.MESSAGE}: Phone or email`,
      code: SystemCode.INVALID_PARAMS.CODE
    })
  }
  if (!userInfo.id) {
    return res.status(400).send({
      status: 'Error',
      message: 'User undefined'
    })
  }
  try {
    let user = await User.updateUser(userInfo)
    res.status(200).send({
      status: Status.STATUS_SUCCESS,
      message: SystemCode.NO_ERROR.MESSAGE,
      code: SystemCode.NO_ERROR.CODE,
      userId: user.id
    })
  } catch (error) {
    res.status(403).send({
      status: Status.STATUS_ERROR,
      code: SystemCode.UNKNOWN_ERROR.CODE,
      message: SystemCode.UNKNOWN_ERROR.MESSAGE,
      error
    })
  }
}
module.exports.getUserById = async function (req, res) {
  const { userId } = req.query
  if (!userId) {
    res.status(400).send({
      status: Status.STATUS_ERROR,
      message: SystemCode.INVALID_USER_ID.MESSAGE,
      code: SystemCode.INVALID_USER_ID.CODE
    })
    return
  }
  try {
    let user = await User.getUserById(userId)
    if (user) {
      user.password = undefined
      user.token = undefined
      res.status(200).send({
        status: Status.STATUS_SUCCESS,
        code: SystemCode.NO_ERROR.CODE,
        message: SystemCode.NO_ERROR.MESSAGE,
        data: user
      })
    } else {
      res.status(200).send({
        status: Status.STATUS_ERROR,
        message: Status.USER_NOT_EXISTED.MESSAGE,
        code: SystemCode.USER_NOT_EXISTED.CODE,
      })
    }
  } catch (error) {
    res.status(400).send({
      status: Status.STATUS_ERROR,
      code: SystemCode.USER_NOT_EXISTED.CODE,
      message: SystemCode.USER_NOT_EXISTED.MESSAGE,
      error: error,
    })
  }
}
module.exports.getProfile = async function (req, res) {
  let { userId } = req.query
  if (!userId) {
    res.status(400).send({
      status: Status.STATUS_ERROR,
      message: SystemCode.INVALID_USER_ID.MESSAGE,
      code: SystemCode.INVALID_USER_ID.CODE
    })
    return
  }
  try {
    let user = await User.getProfile(userId)
    res.status(200).send({
      status: Status.STATUS_SUCCESS,
      message: SystemCode.NO_ERROR.MESSAGE,
      code: SystemCode.NO_ERROR.CODE,
      data: user
    })
  } catch (error) {
    res.status(400).send({
      status: Status.STATUS_ERROR,
      message: `${SystemCode.UNKNOWN_ERROR.MESSAGE}: Get Profile Error`,
      code: SystemCode.UNKNOWN_ERROR.CODE,
      error
    })
  }
}
module.exports.getUsers = async function (req, res) {
  let query = req.query
  try {
    let users = await User.getUsers(query)
    res.status(200).send({
      status: Status.STATUS_SUCCESS,
      code: SystemCode.NO_ERROR.CODE,
      message: SystemCode.NO_ERROR.MESSAGE,
      data: users
    })
  } catch (error) {
    res.status(401).send({
      status: Status.STATUS_ERROR,
      message: SystemCode.GET_LIST_USER_EROR.MESSAGE,
      code: SystemCode.GET_LIST_USER_EROR.CODE,
      error
    })
  }
}
module.exports.deleteUser = async function (req, res) {
  let { userId } = req.query
  if (!userId) {
    return res.status(400).send({
      status: Status.STATUS_ERROR,
      message: SystemCode.INVALID_USER_ID.MESSAGE,
      code: SystemCode.INVALID_USER_ID.CODE
    })
  }
  try {
    let result = User.deleteUser(userId)
    res.status(200).send({
      status: Status.STATUS_SUCCESS,
      message: SystemCode.NO_ERROR.MESSAGE,
      code: SystemCode.NO_ERROR.CODE,
      userId: result.id
    })
  } catch (error) {
    res.status(401).send({
      status: Status.STATUS_ERROR,
      message: SystemCode.UNKNOWN_ERROR.MESSAGE,
      code: SystemCode.UNKNOWN_ERROR.CODE.CODE,
      error
    })
  }
}