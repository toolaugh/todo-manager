var express = require('express')
var router = express.Router()
var middleware = require('../../middleware/required_authen')
/* controller */
var userController = require('./user.controller')
var authenController = require('./user.authen')

/* GET users listing. */
router.get('/', function (req, res) {
    res.send('respond with a resource')
})

router
    .post('/user/login', authenController.login)
    .post('/user/register', authenController.register)

router
    .get('/users', middleware.requiredAuthen, userController.getUsers)
    .post('/users/update', middleware.requiredAuthen, userController.updateUsers)

router
    .get('/user', middleware.requiredAuthen, userController.getUserById)
    .put('/user/update', middleware.requiredAuthen, userController.updateUser)
    .delete('/user/delete', middleware.requiredAuthen, userController.deleteUser)

router
    .get('/user/profile', middleware.requiredAuthen, userController.getProfile)



module.exports = router
