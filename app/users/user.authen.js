var { validateEmail, validatePhone } = require('../../utils')
var User = require('./user.model')
var SystemCode = require('../../constants/SystemCode')
var Status = require('../../constants/SystemConstants')

module.exports.login = function (req, res) {
  console.log(req.body)
  const { email, password } = req.body
  if (!email || !password || (email && !validateEmail(email))) {
    return res.status(401).send({
      status: Status.STATUS_ERROR,
      code: SystemCode.INVALID_PARAMS.CODE,
      message: SystemCode.INVALID_PARAMS.MESSAGE
    })
  }
  User.findOne({ email }, function (error, user) {
    if (error) {
      return res.status(400).send({
        status: Status.STATUS_ERROR,
        code: SystemCode.UNKNOWN_ERROR.CODE,
        message: SystemCode.UNKNOWN_ERROR.MESSAGE
      })
    }
    if (user) {
      user.comparePassword(password)
        .then(isMatch => {
          if (!isMatch) {
            return res.status(400).send({
              status: Status.STATUS_ERROR,
              code: SystemCode.INVALID_PARAMS.CODE,
              message: SystemCode.INVALID_PARAMS.MESSAGE
            })
          }
          user.password = undefined
          res.status(200).send({
            status: Status.STATUS_SUCCESS,
            code: SystemCode.NO_ERROR.CODE,
            message: SystemCode.NO_ERROR.MESSAGE,
            data: { token: user.token }
          })
        })
        .catch(error => {
          return res.status(400).send({
            status: Status.STATUS_SUCCESS,
            code: SystemCode.UNKNOWN_ERROR.CODE,
            message: SystemCode.UNKNOWN_ERROR.MESSAGE,
            error
          })
        })
    } else {
      res.status(400).send({
        status: Status.STATUS_ERROR,
        code: SystemCode.USER_NOT_EXISTED.CODE,
        message: SystemCode.USER_NOT_EXISTED.MESSAGE
      })
    }
  })
}
module.exports.register = function (req, res) {
  const { username, email, password, phoneNumber } = req.body
  if (!username || !email || !password || (phoneNumber && !validatePhone(phoneNumber)) || (email && !validateEmail(email))) {
    return res.status(401).send({
      status: Status.STATUS_ERROR,
      code: SystemCode.INVALID_PARAMS.CODE,
      message: SystemCode.INVALID_PARAMS.MESSAGE
    })
  } else {
    User.getUserByEmail(email)
      .then(user => {
        if (user) {
          return res.status(401).send({
            status: Status.STATUS_ERROR,
            code: SystemCode.USER_EXISTED.CODE,
            message: SystemCode.USER_EXISTED.MESSAGE
          })
        } else {
          const nUser = new User(req.body)
          nUser.save(function (error, user) {
            if (error) {
              return res.status(401).send({
                status: Status.STATUS_ERROR,
                code: SystemCode.UNKNOWN_ERROR.CODE,
                message: SystemCode.UNKNOWN_ERROR.MESSAGE
              })
            } else {
              return res.status(200).json({
                status: Status.STATUS_SUCCESS,
                code: SystemCode.NO_ERROR.CODE,
                message: SystemCode.NO_ERROR.MESSAGE,
                data: {
                  token: user.token,
                  userId: user.id
                },
              })
            }
          })
        }
      })
      .catch(error => {
        return res.status(400).send({
          status: Status.STATUS_ERROR,
          code: SystemCode.UNKNOWN_ERROR.CODE,
          message: SystemCode.UNKNOWN_ERROR.MESSAGE,
          error,
        })
      })
  }
}
module.exports.resetPassword = function (req, res) {
  res.send({ reset: 'sssss' })
}
