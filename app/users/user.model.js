var mongoose = require('mongoose')
var bcrypt = require('bcrypt-nodejs')
var _ = require('lodash')
var {
  getTokenForUser,
  generateUserId
} = require('../../utils')
var Schema = mongoose.Schema

/* user Schema */

var UserSchema = new Schema({
  id: {
    type: String,
    default: null
  },
  username: {
    type: String,
    trim: true,
    default: null,
  },
  displayName: {
    type: String,
    default: null
  },
  password: {
    type: String,
    trim: true,
  },
  email: {
    type: String,
    lowercase: true,
    default: null,
  },
  token: {
    type: String,
    default: null,
  },
  phoneNumber: {
    type: String,
    default: null,
  },
  avatar: {
    type: String,
    default: null
  },
  coverPicture: {
    type: String,
    default: null,
  },
  address: {
    type: String,
    default: null,
  },
  role: {
    type: String,
    default: null
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  isBlocked: {
    type: Boolean,
    default: false,
  },
  dateOfBirth: {
    type: Date,
    default: null,
  },
  gender: {
    type: String,
    default: null,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  }
})

/* hash password before save */
UserSchema.pre('save', function (next) {
  const user = this
  bcrypt.genSalt(10, (error, salt) => {
    if (error) {
      return next(error)
    }
    bcrypt.hash(user.password, salt, null, (error, hash) => {
      if (error) {
        return next(error)
      }
      user.id = generateUserId() || 0
      user.password = hash
      user.token = getTokenForUser(user)
      next()
    })
  })
})
UserSchema.methods.comparePassword = function (candidatePassword) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(candidatePassword, this.password, function (error, isMatch) {
      if (error) {
        return reject(error)
      }
      return resolve(isMatch)
    })
  })
}
UserSchema.statics.getUserById = function (userId) {
  return new Promise((resolve, reject) => {
    this.findOne({
      id: userId
    }, function (error, user) {
      if (error) {
        reject(error)
      } else {
        if (user) {
          user.token = undefined
          user.password = undefined
        }
        resolve(user)
      }
    })
  })
}
UserSchema.statics.getUserByEmail = function (email = '') {
  return new Promise((resolve, reject) => {
    this.findOne({
      email
    }, function (error, user) {
      if (error) {
        reject(error)
      } else {
        resolve(user)
      }
    })
  })
}
UserSchema.statics.updateUser = function (user = {}) {
  console.log('user', user)
  return new Promise((resolve, reject) => {
    this.findOneAndUpdate({
      id: user.id
    }, user, function (error, result) {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}
UserSchema.statics.updateUsers = function (userIds = [], data = {}) {
  return new Promise((resolve, reject) => {
    this.findOneAndUpdate({
      id: {
        $in: userIds
      },
      data
    }, function (error, results) {
      if (error) {
        reject(error)
      } else {
        resolve(results)
      }
    })
  })
}
UserSchema.statics.getUsers = function (params = {}) {
  console.log(params)
  let query = this.find()

  let limit = params.limit || 10
  let perPage = params.perPage || 0
  let isDeleted = params.isDeleted || false
  let isActive = params.isActive || true

  query = query.where('isDeleted').equals(isDeleted)
  query = query.where('isActive').equals(isActive)

  if (limit && perPage) {
    query = query.skip(limit * (perPage - 1)).limit(limit)
  }

  return new Promise((resolve, reject) => {
    query.exec(function (error, users) {
      if (error) {
        reject(error)
      } else {
        _.forEach(users, function (user) {
          user.token = undefined
          user.password = undefined
        })
        resolve(users)
      }
    })
  })
}
UserSchema.statics.getProfile = function (userId = '') {
  return new Promise((resolve, reject) => {
    this.findOne({
      id: userId
    }, function (error, user) {
      if (error) {
        reject(error)
      } else {
        if (user) {
          user.token = undefined
          user.password = undefined
        }
        resolve(user)
      }
    })
  })
}
UserSchema.statics.checkUserExisted = function (userIds = []) {
  return new Promise((resolve, reject) => {
    this.countDocuments({
      _id: {
        $in: userIds
      },
      isActive: true
    }, function (error, count) {
      if (error) {
        reject(error)
      }
      if (count > 0) {
        resolve(true)
      } else {
        resolve(false)
      }
    })
  })
}
UserSchema.statics.deleteUser = function (userId = '') {
  return new Promise((resolve, reject) => {
    this.findOneAndUpdate({
      id: userId
    }, {
      isDeleted: true
    }, function (error, result) {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}
module.exports = mongoose.model('User', UserSchema)