var mongoose = require('mongoose')
var Schema = mongoose.Schema;

const taskSchema = new Schema({
  taskTitle: {
    type: String,
    default: null,
    required: true
  },
  taskDescription: {
    type: String,
    default: null
  },
  startDate: {
    type: Date,
    default: Date.now
  },
  dueDate: {
    type: Date,
    default: Date.now
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  // status : 1 new, 2: completed , 3 rejected
  status: {
    type: String,
    default: 1,
  },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  isDraft: {
    type: Boolean,
    default: false,
  },
  assignedTo: [{
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  }],
  comments: [{
    type: Schema.Types.ObjectId,
    ref: 'Comments',
    default: null
  }]
})
taskSchema.statics.createTask = function (data = {}) {
  let Task = this
  let nTask = new Task(data)
  return new Promise((resolve, reject) => {
    nTask.save(function (error, result) {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}
taskSchema.statics.getTaskByCode = function (code = '') {
  return new Promise((resolve, reject) => {
    this
      .findOne({
        code
      })
      .populate('creator', 'id username displayName phoneNumber avatar role address')
      .populate('assignedTo', 'id username displayName phoneNumber avatar role address')
      .exec(function (error, result) {
        if (error) {
          reject(error)
        } else {
          resolve(result)
        }
      })
  })
}
taskSchema.statics.getTaskById = function (id) {
  return new Promise((resolve, reject) => {
    this.findOne({
        _id: id
      })
      .populate('creator', 'id username displayName phoneNumber avatar role address')
      .populate('assignedTo', 'id username displayName phoneNumber avatar role address')
      .exec(function (error, result) {
        if (error) {
          reject(error)
        } else {
          resolve(result)
        }
      })
  })
}

taskSchema.statics.deleteTaskById = function (id = '') {
  return new Promise((resolve, reject) => {
    this.findOneAndRemove({
      _id: id
    }, function (error, result) {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}

taskSchema.statics.updateTask = function (id, data) {
  return new Promise((resolve, reject) => {
    this.findOneAndUpdate({
      _id: id
    }, data, function (error, result) {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}

taskSchema.statics.listTasks = function (params = {}) {
  let query = this.find()
  let perPage = params.perPage || 1
  let limit = params.limit || 10
  if (perPage && limit) {
    query = query.skip(limit * (perPage - 1)).limit(limit)
  }
  query = query.populate('creator', 'id username displayName phoneNumber avatar role address')
  query = query.populate('assignedTo', 'id username displayName phoneNumber avatar role address')

  return new Promise((resolve, reject) => {
    query.exec((error, result) => {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}
taskSchema.statics.assignTask = function (params) {

}
module.exports = mongoose.model('Tasks', taskSchema, 'tasks')