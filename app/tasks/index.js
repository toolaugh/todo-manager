var express = require('express')
var router = express.Router()
var middleware = require('../../middleware/required_authen')
/* controller */
var taskController = require('./task.controller')

/* GET users listing. */
router.get('/', function (req, res) {
    res.send('respond with a resource')
})


router
    .get('/tasks/list', middleware.requiredAuthen, taskController.listTasks)
    .post('/tasks/update', middleware.requiredAuthen, taskController.updateTasks)
    .post('/task/assign', middleware.requiredAuthen, taskController.assignTask)
router
    .post('/task/create', middleware.requiredAuthen, taskController.createTask)
    .get('/task', middleware.requiredAuthen, taskController.getTaskById)
    .put('/task/update', middleware.requiredAuthen, taskController.updateTask)
    .delete('/task/delete', middleware.requiredAuthen, taskController.deleteTask)

module.exports = router