var _ = require('lodash');
var Tasks = require('./task.model')
var Mongoose = require('mongoose');

const ObjectId = Mongoose.Types.ObjectId

module.exports.createTask = async function (req, res) {
  let reqBody = req.body
  let errors = {}

  if (!reqBody.taskTitle) {
    errors.title = 'Invalid title'
  }

  if (!reqBody.taskDescription) {
    errors.description = "Description is null"
  }

  if (!reqBody.creator) {
    errors.creator = 'Invalid params'
  }

  if (Object.keys(errors).length > 0) {
    return res.status(400).send({
      status: 'Error',
      message: 'Invalid Params'
    })
  } else {
    try {
      const task = await Tasks.createTask(reqBody);
      res.status(200).send({
        status: 'Success',
        data: task
      })
    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong',
        error
      })
    }
  }
}

module.exports.updateTask = async function (req, res) {
  let task = req.body;
  if (!task._id || !!task.creator) {
    res.status(400).send({
      status: "Error",
      message: 'Invalid params'
    })
  } else {
    try {

    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong',
        error
      })
    }
  }
}

module.exports.deleteTask = async function (req, res) {
  let taskId = req.query.taskId;
  console.log(req.query);
  if (!taskId) {
    return res.status(400).send({
      status: "Error",
      message: "Invalid params"
    })
  } else {
    try {
      let result = await Tasks.deleteTaskById(taskId);
      res.status(200).send({
        status: 'Success',
        message: 'Delete task successfully !!',
        data: {
          taskId: result._id
        }
      })
    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong',
        error
      })
    }

  }
}
module.exports.listTasks = async function (req, res) {
  let params = req.query;
  try {
    let tasks = await Tasks.listTasks(params);
    res.status(200).send({
      status: 'Success',
      message: 'Get tasks successfully',
      data: {
        tasks,
      }
    })
  } catch (error) {
    return
  }
}
module.exports.getTaskById = async function (req, res) {
  let taskID = req.query.taskId
  if (!taskID) {
    res.status(400).send({
      status: 'Error',
      message: 'Invalid params',
      data: null
    })
  } else {
    try {
      let response = await Tasks.getTaskById(taskID);
      res.status(200).send({
        status: 'Success',
        message: 'Get task success',
        data: response
      })
    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong',
        error: error.message
      })
    }
  }
  res.status(200).send({
    success: 'sss'
  })
}
module.exports.updateTask = async function (req, res) {
  let data = req.body;
  if (!data._id) {
    res.status(400).send({
      status: "Error",
      message: 'Invalid params'
    })
  } else {
    try {
      const response = await Tasks.updateTask(data._id, data);
      res.status(200).send({
        status: 'Success',
        data: {
          _id: response._id
        }
      })
    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong, try again',
        error
      })
    }

  }
}
module.exports.updateTasks = async function (req, res) {
  res.status(200).send({
    success: 'sss'
  })
}
module.exports.assignTask = async function (req, res) {
  let assignedUsers = req.body.assignedUsers;
  let taskID = req.body.taskID;

  if (!taskID) {
    return res.status(400).send({
      status: 'Error',
      message: 'Invalid params',
      data: []
    })
  } else {
    try {
      let task = await Tasks.getTaskById(taskID)
      if (!task) {
        res.status(400).send({
          status: 'Error',
          message: 'Task assigned not found, try again',
        })
      } else {
        task.assignedTo = [...task.assignedTo, ...assignedUsers];
        task.assignedTo = _.uniq(task.assignedTo);

        task.save(function (error, result) {
          if (!error) {
            res.status(200).send({
              status: 'Success',
              message: 'Assigned task success',
              data: {
                assigned: assignedUsers,
                result
              }
            })
          } else {
            res.status(400).send({
              status: 'Error',
              message: 'Something went wrong, try again !',
              data: null
            })
          }
        })
      }
    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong',
        error: error.message,
      })
    }
  }
}