var express = require('express')
var router = express.Router()
var config = require('../configs')

/* routes */

var user_route = require('./users')
var task_route = require('./tasks')
// var comment_route = require('./comments')

/* GET home page. */
router.get('/', function (req, res, next) {
    res.json({ index: 'Connected' })
})
router.get('/api/v1', function (req, res, next) {
    res.status(200).send({ status: 'success', API: 'pending' })
})

router.use(`/api/${config.API_VERSION}`, user_route)
router.use(`/api/${config.API_VERSION}`, task_route)

module.exports = router