var _ = require('lodash');
var Comments = require('./comment.model');
var Tasks = require('../tasks/task.model');
var ObjectId = require('mongoose').Schema.Types.ObjectId;

module.exports.createComment = async function (req, res) {
  let reqBody = req.body;
  if (!reqBody.creator || !req.taskId) {
    return res.status(400).send({
      status: 'Error',
      message: 'Invalid params'
    })
  } else {
    try {
      // create comments
      let result = await Comment.createComment(reqBody);
      // add comment to tasks;
      let task = await Tasks.getTaskById(reqBody.taskId);
      if (task) {
        task.comments = [...task.comments, ...result._id]
        task.comments = _.uniq(task.comments);
        task.save(function (error, result) {
          if (!error) {
            res.status(200).send({
              status: 'Success',
              message: 'Create comment successfully',
              data: result
            })
          }
        })
      }
    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong !',
        error
      })
    }
  }
}

module.exports.getCommentById = async function (req, res) {
  let query = req.query

  if (!query.commentId) {
    res.status(400).send({
      status: 'Error',
      message: 'Invalid params'
    })
  } else {
    try {
      let result = await Comments.getCommentById(query.commentId);
      res.status(200).send({
        status: 'Success',
        message: 'Get comment success',
        data: result
      })
    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong',
        error
      })
    }
  }
}

module.exports.listComments = async function (req, res) {
  let query = req.query;
  if (!query.taskId) {
    return res.status(400).send({
      status: 'Error',
      message: 'Invalid params'
    })
  } else {
    try {
      let comments = await Comments.listComments(query)
      res.status(200).send({
        status: 'Success',
        message: 'List comments success',
        data: comments
      })
    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong',
        error
      })
    }
  }
}
module.exports.updateComment = async function (req, res) {
  let reqBody = req.body;
  if (!reqBody._id || !reqBody.taskId) {
    res.status(200).send({
      status: 'Error',
      message: 'Invalid params',
    })
  } else {
    try {
      let result = await Comments.updateComment(reqBody)
      res.status(200).send({
        status: 'Success',
        message: 'Update message success',
        data: result
      })

    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong',
        error
      })
    }
  }
}
module.exports.replyComment = async function (req, res) {
  let reqBody = req.body;
  if (!reqBody.parentId || !reqBody.taskId || !reqBody.creator || !reqBody.content) {
    res.status(400).send({
      status: 'Error',
      message: 'Invalid params',
      data: null
    })
  } else {
    try {
      const [newComment, task] = await Promise.all(Comments.createComment(reqBody), Tasks.getTaskById(taskId))
      task.creator = reqBody.creator;
      task.parentId = reqBody.parentId;
      task.replies.push(newComment._id);
      res.status(200).send({
        status: 'Success',
        message: 'Reply success',
        data: newComment
      })
    } catch (error) {
      res.status(400).send({
        status: 'Error',
        message: 'Something went wrong, try again !',
        error: error.message
      })
    }

  }
}