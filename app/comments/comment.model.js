var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const commentSchema = new Schema({
  content: {
    type: String,
    default: null,
  },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'Users',
    default: null
  },
  timestamp: {
    type: Date,
    default: Date.now
  },
  numberOfLikes: {
    type: Number,
    default: 0
  },
  parentId: {
    type: Schema.Types.ObjectId,
    ref: 'Comments',
    default: null,
  },
  replies: [{
    type: Schema.Types.ObjectId,
    ref: 'Comments',
    default: null,
  }]
})

commentSchema.static.createComment = function (comment) {
  let Comment = this
  let nComment = new Comment(comment)
  return new Promise((resolve, reject) => {
    nComment.save(function (error, result) {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}

commentSchema.statics.getCommentByCode = function (code = '') {
  return new Promise((resolve, reject) => {
    this.findOne({
        code
      })
      .populate('creator', 'id username displayName phoneNumber avatar role address')
      .populate('commentId')
      .exec((error, result) => {
        if (error) {
          reject(error)
        } else {
          resolve(result)
        }
      })
  })
}

commentSchema.statics.getCommentById = function (id) {
  return new Promise((resolve, reject) => {
    this.findOne({
        _id: id
      })
      .populate('creator', 'id username displayName phoneNumber avatar role address')
      .populate('commentId')
      .exec((error, result) => {
        if (error) {
          reject(error)
        } else {
          resolve(result)
        }
      })
  })
}

commentSchema.statics.deleteCommentById = function (id = '') {
  return new Promise((resolve, reject) => {
    this.findOneAndRemove({
      _id: id
    }, function (error, result) {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}

commentSchema.statics.updateComment = function (id, data) {
  return new Promise((resolve, reject) => {
    this.findOneAndUpdate({
      _id: id
    }, data, function (error, result) {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}

commentSchema.statics.listComments = function (params = {}) {
  let query = this.find()
  let perPage = params.perPage || 1
  let limit = params.limit || 10

  query = query.where('taskId').in([params.taskId])

  if (perPage && limit) {
    query = query.skip(limit * (perPage - 1)).limit(limit)
  }

  if (params.creator) {
    query = query.populate(query.creator).select('id username displayName phoneNumber avatar role address')
  }

  return new Promise((resolve, reject) => {
    query.exec((error, result) => {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
  })
}
module.exports = mongoose.model('Comments', commentSchema, 'comments')