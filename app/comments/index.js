var express = require('express')
var router = express.Router()
var middleware = require('../../middleware/required_authen')
/* controller */
var commentController = require('./comment.controller')

/* GET users listing. */
router.get('/', function (req, res) {
    res.send('respond with a resource')
})


router
    .get('/comments/list', middleware.requiredAuthen, commentController.listComments)
    .post('/comments/update', middleware.requiredAuthen, commentController.updateComments)

router
    .post('/comment/create', middleware.requiredAuthen, commentController.createComment)
    .get('/comment', middleware.requiredAuthen, commentController.getCommentById)
    .put('/comment/update', middleware.requiredAuthen, commentController.updateComment)
    .delete('/comment/delete', middleware.requiredAuthen, commentController.deleteComment)

module.exports = router
