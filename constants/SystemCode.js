exports.NO_ERROR = {
    CODE: 0,
    MESSAGE: 'Do success'
}

/* authorization */
exports.NOT_LOGIN = {
    CODE: 1,
    MESSAGE: 'User not logged in !'
}
exports.NOT_REGISTER ={
    CODE: 2,
    MESSAGE: 'User not registered !'
}
exports.USER_EXISTED = {
    CODE: 3,
    MESSAGE: 'User existed !'
}
exports.USER_NOT_EXISTED = {
    CODE: 4,
    MESSAGE: 'User not existed !'
}
exports.INVALID_PARAMS = {
    CODE: 5,
    MESSAGE: 'Invalid params !'
}
exports.UNKNOWN_ERROR = {
    CODE: 6,
    MESSAGE: 'Unknown error !'
}
exports.INVALID_USER_ID =  {
    CODE: 7,
    MESSAGE: 'Invalid UserId'
}
exports.GET_LIST_USER_EROR = {
    CODE: 8,
    MESSAGE: 'Query users error'
}
exports.CREATE_TEST_ERROR = {
    CODE: 9,
    MESSAGE: 'Create test error'
}
exports.INVALID_TEST_ID = {
    MESSAGE: 'Invalid Test Id',
    CODE: 10
}
exports.DELETE_TEST_ERROR = {
    CODE: 11,
    MESSAGE: 'Delete test error'
}
exports.GET_LIST_TEST_ERROR = {
    CODE: 12,
    MESSAGE: 'Get list tests error'
}
exports.DELETE_CLASSS_ERROR = {
    CODE: 13,
    MESSAGE: 'Delete class error'
}
exports.CREATE_CLASSS_ERROR = {
    CODE: 14,
    MESSAGE: 'Create Class Error'
}
exports.UPDATE_CLASS_ERROR = {
    CODE: 15,
    MESSAGE: 'Update Class Error'
}
exports.GET_CLASS_ERROR = {
    CODE: 16, 
    MESSAGE: 'Get Class Error'
}
exports.CLASS_EXISTED = {
    MESSAGE: 'Class existed',
    CODE: 17
}
exports.CREATE_SUBJECT_ERROR = {
    CODE: 18,
    MESSAGE: 'Create subject error'
}
exports.SUBJECT_EXISTED = {
    MESSAGE: 'Subject is existed ',
    CODE: 19
}
exports.GET_SUBJECT_ERROR = {
    MESSAGE: 'Get subject errors',
    CODE: 20
}
exports.DELETE_SUBJECT_ERROR = {
    MESSAGE: 'Delete subject error',
    CODE: 21
}
exports.UPDATE_SUBJECT_ERROR = {
    MESSAGE: 'Update Subject error',
    CODE: 22
}
exports.CREATE_QUESTION_ERROR = {
    MESSAGE: 'Create Question Error',
    CODE: 23
}
exports.UPDATE_QUESTION_ERROR = {
    MESSAGE: 'Update Question Error',
    CODE: 24
}
exports.DELETE_QUESTION_ERROR = {
    MESSAGE: 'Delete Question Error',
    CODE: 25
}
exports.GET_QUESTION_ERROR = {
    MESSAGE: 'Get Question Error',
    CODE: 26
}
exports.QUESTION_NOT_EXISTED = {
    MESSAGE: 'Question not existed',
    CODE: 27
}
exports.TEST_NOT_EXISTED = {
    MESSAGE: 'Test not existed',
    CODE: 29
}
exports.GET_BOOKMARK_ERROR = {
    MESSAGE: 'Get bookmark error',
    CODE: 30
}
exports.CREATE_COLLECTION_ERROR = {
    MESSAGE: 'Create Collection error',
    CODE: 31
}
exports.UPDATE_COLLECTION_ERROR = {
    MESSAGE: 'Update Collection error',
    CODE: 31
}
exports.DELETE_COLLECTION_ERROR = {
    MESSAGE: 'Delete Collection error',
    CODE: 31
}