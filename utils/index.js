var jwt = require('jsonwebtoken')
var config = require('../configs')

module.exports.getTokenForUser = function (user) {
    return jwt.sign({ user: user }, config.JWT_SECRET_KEY)
}

module.exports.validateEmail = function (email) {
    let reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return reg.test(String(email).toLocaleLowerCase())
}
module.exports.validatePhone = function(phone) {
    let regPhone = /^\+?\d{8,15}$/
    return regPhone.test(phone)
}
module.exports.generateUserId = function () {
    var _getRandomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min
    }
    this.timestamp = +new Date
    var generate = function() {
        var ts = this.timestamp.toString()
        var parts = ts.split('').reverse()
        var id = ''
        
        for( var i = 0; i < 9; ++i ) {
            var index = _getRandomInt( 0, parts.length - 1 )
            id += parts[index]	 
        }
        console.log(id)
        return id
    }
    return generate()
}
