var jwt = require('jsonwebtoken')

var User  = require('../app/users/user.model')
var config = require('../configs')
var SystemCode = require('../constants/SystemCode')
var Status  = require('../constants/SystemConstants')

exports.requiredAuthen = function(req, res, next) {
    var token = req.headers['x-access-token'] || req.cookies['jwtToken'] || req.body.authenToken || req.query.authenToken
    if(token && token !== 'token') {
        jwt.verify(token, config.JWT_SECRET_KEY, function(error, decoded) {
            if(error) {
                res.status(400).send({
                    status: Status.STATUS_ERROR,
                    code: SystemCode.NOT_LOGIN.CODE,
                    message: 'Something went wrong, when decode ...',
                    error,
                })
            } else {
                if(decoded) {
                    var email = decoded.user.email
                    User.getUserByEmail(email)
                        .then(user => {
                            if(!user){
                                User.create(decoded, function(error, nUser) {
                                    if(error) {
                                        return res.status(401).send({
                                            status: Status.STATUS_ERROR,
                                            code: SystemCode.NOT_LOGIN.CODE,
                                            message: SystemCode.NOT_LOGIN.MESSAGE,
                                        })
                                    } else {
                                        req.user = nUser
                                        next()
                                    }
                                })
                            } else{
                                req.user = user
                                next()
                            }
                        })
                        .catch(error => {
                            return res.status(401).send({
                                status: Status.STATUS_ERROR,
                                code: SystemCode.NOT_LOGIN.CODE,
                                message: SystemCode.NOT_LOGIN.MESSAGE,
                                error,
                            })
                        })
                } else{
                    res.status(401).send({
                        status: Status.STATUS_ERROR,
                        code: SystemCode.NOT_LOGIN.CODE,
                        message: SystemCode.NOT_LOGIN.MESSAGE,
                    })
                }
            }
        })
    } else {
        res.status(401).send({
            status: Status.STATUS_ERROR,
            code: SystemCode.NOT_LOGIN.CODE,
            message: SystemCode.NOT_LOGIN.MESSAGE,
        })
    }
}